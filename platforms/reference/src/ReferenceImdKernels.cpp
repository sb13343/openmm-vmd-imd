/* -------------------------------------------------------------------------- *
 *                                   OpenMM                                   *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2016 Stanford University and the Authors.           *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

#include "ReferenceImdKernels.h"
#include "ImdForce.h"
#include "openmm/OpenMMException.h"
#include "openmm/NonbondedForce.h"
#include "openmm/internal/ContextImpl.h"
#include "openmm/reference/RealVec.h"
#include "openmm/reference/ReferencePlatform.h"
#include "ImdKernels.h"
#include <cstring>
#include "openmm/Units.h"
#include <iostream>
#include <debug.h>

using namespace ImdPlugin;
using namespace OpenMM;
using namespace std;

static vector<RealVec>& extractPositions(ContextImpl& context) {
    ReferencePlatform::PlatformData* data = reinterpret_cast<ReferencePlatform::PlatformData*>(context.getPlatformData());
    return *((vector<RealVec>*) data->positions);
}

static vector<RealVec>& extractForces(ContextImpl& context) {
    ReferencePlatform::PlatformData* data = reinterpret_cast<ReferencePlatform::PlatformData*>(context.getPlatformData());
    return *((vector<RealVec>*) data->forces);
}

static RealVec* extractBoxVectors(ContextImpl& context) {
    ReferencePlatform::PlatformData* data = reinterpret_cast<ReferencePlatform::PlatformData*>(context.getPlatformData());
    return (RealVec*) data->periodicBoxVectors;
}

ReferenceCalcImdForceKernel::ReferenceCalcImdForceKernel(std::string name, const OpenMM::Platform& platform, OpenMM::ContextImpl& contextImpl) : CalcImdForceKernel(name, platform), contextImpl(contextImpl), lastStepIndex(0) {
    IF_DEBUG std::cerr << "IMD Debug: Creating reference imd force implementation" << std::endl;
}

ReferenceCalcImdForceKernel::~ReferenceCalcImdForceKernel() {
}

void ReferenceCalcImdForceKernel::initialize(const System& system, const ImdForce& force) {
    IF_DEBUG std::cerr << "IMD Debug: paused at initialise  " << isPaused() << std::endl;

    initializeImd(system, force);
    usesPeriodic = system.usesPeriodicBoundaryConditions();

    positionsToSend.resize(system.getNumParticles() * 3);
    IF_DEBUG std::cerr << "IMD Debug: paused at end of initialise  " << isPaused() << std::endl;


}

double ReferenceCalcImdForceKernel::execute(ContextImpl& context, bool includeForces, bool includeEnergy) {

    IF_DEBUG std::cerr << "IMD Debug: paused at step " << isPaused() << std::endl;

    if(!imd.isClientConnected())
    {
        auto connected = imd.tryConnect(waitForConnection);
        if(connected)
            paused = false;
    }


    auto* data = reinterpret_cast<ReferencePlatform::PlatformData*>(context.getPlatformData());
    int step = data->stepCount;

    IF_DEBUG std::cerr << "IMD Debug: step: " << step << std::endl;

    if(step != lastStepIndex)
    {
        lastStepIndex = step;
        processImdInput();
    }

    vector<RealVec>& openmmPositions = extractPositions(context);
    vector<RealVec>& openmmForces = extractForces(context);

    if(lastStepIndex % rate == 0)
    {
        IF_DEBUG std::cerr << "IMD Debug: transmitting positions  " << std::endl;

        updatePositionsToSend(openmmPositions);
        sendEnergies(data);
        sendPositions();
    }

    IF_DEBUG std::cerr << "IMD Debug: applying forces  " << std::endl;

    applyReceivedForces(openmmForces);
    //TODO IMD force produces no energy contribution...
    return 0;
}

void ReferenceCalcImdForceKernel::applyReceivedForces(std::vector<Vec3>& openmmForces)
{
    for(int i=0; i < receivedAtomIndices.size(); i++)
    {
        openmmForces[receivedAtomIndices[i]][0] += receivedForces[i * 3 + 0] * forceConversion;
        openmmForces[receivedAtomIndices[i]][1] += receivedForces[i * 3 + 1] * forceConversion;
        openmmForces[receivedAtomIndices[i]][2] += receivedForces[i * 3 + 2] * forceConversion;
    }
}

void ReferenceCalcImdForceKernel::sendEnergies(ReferencePlatform::PlatformData *data) {
    IMDEnergies energies;
    energies.tstep = data->stepCount;
    imd.sendEnergies(energies);
}
