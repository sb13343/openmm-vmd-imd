/* -------------------------------------------------------------------------- *
 *                                   OpenMM                                   *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2016 Stanford University and the Authors.           *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

#include "CudaImdKernels.h"
#include "CudaImdKernelSources.h"
#include "openmm/NonbondedForce.h"
#include "openmm/internal/ContextImpl.h"
#include "openmm/internal/ThreadPool.h"
#include "openmm/cuda/CudaBondedUtilities.h"
#include "openmm/cuda/CudaForceInfo.h"
#include <cstring>
#include <map>

using namespace ImdPlugin;
using namespace OpenMM;
using namespace std;

class CudaCalcImdForceKernel::StartCalculationPreComputation : public CudaContext::ForcePreComputation {
public:
    StartCalculationPreComputation(CudaCalcImdForceKernel& owner) : owner(owner) {
    }
    void computeForceAndEnergy(bool includeForces, bool includeEnergy, int groups) {
        owner.beginComputation(includeForces, includeEnergy, groups);
    }
    CudaCalcImdForceKernel& owner;
};

class CudaCalcImdForceKernel::ExecuteTask : public CudaContext::WorkTask {
public:
    ExecuteTask(CudaCalcImdForceKernel& owner) : owner(owner) {
    }
    void execute() {
        owner.executeOnWorkerThread();
    }
    CudaCalcImdForceKernel& owner;
};

class CudaCalcImdForceKernel::CopyForcesTask : public ThreadPool::Task {
public:
    CopyForcesTask(CudaContext& cu, vector<Vec3>& forces) : cu(cu), forces(forces) {
    }
    void execute(ThreadPool& threads, int threadIndex) {
        // Copy the forces applied by IMD to a buffer for uploading.  This is done in parallel for speed.
        
        int numParticles = cu.getNumAtoms();
        int numThreads = threads.getNumThreads();
        int start = threadIndex*numParticles/numThreads;
        int end = (threadIndex+1)*numParticles/numThreads;
        if (cu.getUseDoublePrecision()) {
            double* buffer = (double*) cu.getPinnedBuffer();
            for (int i = start; i < end; ++i) {
                const Vec3& p = forces[i];
                buffer[3*i] = p[0];
                buffer[3*i+1] = p[1];
                buffer[3*i+2] = p[2];
            }
        }
        else {
            float* buffer = (float*) cu.getPinnedBuffer();
            for (int i = start; i < end; ++i) {
                const Vec3& p = forces[i];
                buffer[3*i] = (float) p[0];
                buffer[3*i+1] = (float) p[1];
                buffer[3*i+2] = (float) p[2];
            }
        }
    }
    CudaContext& cu;
    vector<Vec3>& forces;
};

class CudaCalcImdForceKernel::AddForcesPostComputation : public CudaContext::ForcePostComputation {
public:
    AddForcesPostComputation(CudaCalcImdForceKernel& owner) : owner(owner) {
    }
    double computeForceAndEnergy(bool includeForces, bool includeEnergy, int groups) {
        return owner.addForces(includeForces, includeEnergy, groups);
    }
    CudaCalcImdForceKernel& owner;
};

CudaCalcImdForceKernel::~CudaCalcImdForceKernel() {
    cu.setAsCurrent();
    if (imdForces != NULL)
        delete imdForces;
    cuStreamDestroy(stream);
    cuEventDestroy(syncEvent);
}

void CudaCalcImdForceKernel::initialize(const System& system, const ImdForce& force) {

    initializeImd(system, force);

    cu.setAsCurrent();
    cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING);
    cuEventCreate(&syncEvent, CU_EVENT_DISABLE_TIMING);
    int elementSize = (cu.getUseDoublePrecision() ? sizeof(double) : sizeof(float));
    imdForces = new CudaArray(cu, 3*system.getNumParticles(), elementSize, "imdForces");
    map<string, string> defines;
    defines["NUM_ATOMS"] = cu.intToString(cu.getNumAtoms());
    defines["PADDED_NUM_ATOMS"] = cu.intToString(cu.getPaddedNumAtoms());
    defines["FORCE_CONVERSION"] = cu.doubleToString((double) forceConversion);
    CUmodule module = cu.createModule(CudaImdKernelSources::imdForce, defines);
    addForcesKernel = cu.getKernel(module, "addForces");
    forceGroupFlag = (1<<force.getForceGroup());
    cu.addPreComputation(new StartCalculationPreComputation(*this));
    cu.addPostComputation(new AddForcesPostComputation(*this));

}

double CudaCalcImdForceKernel::execute(ContextImpl& context, bool includeForces, bool includeEnergy) {
    // This method does nothing.  The actual calculation is started by the pre-computation, continued on
    // the worker thread, and finished by the post-computation.
    
    return 0;
}

void CudaCalcImdForceKernel::beginComputation(bool includeForces, bool includeEnergy, int groups) {
    if ((groups&forceGroupFlag) == 0)
        return;
    int step = cu.getStepCount();
    // Only get the positions off the GPU if the step requires it.
    if(step % rate == 0)
        contextImpl.getPositions(positions);

    // The actual force computation will be done on a different thread.
    
    cu.getWorkThread().addTask(new ExecuteTask(*this));
}

void CudaCalcImdForceKernel::executeOnWorkerThread() {

    // Configure the IMD interface object.
    if(!imd.isClientConnected())
        imd.tryConnect(waitForConnection);

    int numParticles = contextImpl.getSystem().getNumParticles();

    int step = cu.getStepCount();


    bool receivedForces = false;
    if(step != lastStepIndex)
    {
        lastStepIndex = step;
        auto messageType = processImdInput();
        receivedForces = messageType == IMDType::IMD_MDCOMM;
    }


    if(lastStepIndex % rate == 0)
    {
        updatePositionsToSend(positions);
        sendEnergies();
        sendPositions();
    }

    if(receivedForces)
        applyImdForces(numParticles);

}

void CudaCalcImdForceKernel::sendEnergies()
{
    IMDEnergies energies;
    energies.tstep = cu.getStepCount();
    imd.sendEnergies(energies);
}

void CudaCalcImdForceKernel::applyImdForces(int numParticles)
{
    perAtomForces.resize(numParticles);
    memset(&perAtomForces[0], 0, numParticles*sizeof(Vec3));

    //TODO probably not worth parallelizing, but to consider.
    // OpenMM go to great lengths to order arrays nicely for GPUs
    // so scatter the received forces here rather than on the GPU.
    for(int i =0; i < receivedAtomIndices.size(); i++)
    {
        int atomIndex=receivedAtomIndices[i];
        perAtomForces[atomIndex][0] = receivedForces[3*i];
        perAtomForces[atomIndex][1] = receivedForces[3*i+1];
        perAtomForces[atomIndex][2] = receivedForces[3*i+2];

    }

    // Upload the forces to the device.

    CopyForcesTask task(cu, perAtomForces);
    cu.getPlatformData().threads.execute(task);
    cu.getPlatformData().threads.waitForThreads();
    cu.setAsCurrent();
    cuMemcpyHtoDAsync(imdForces->getDevicePointer(), cu.getPinnedBuffer(), imdForces->getSize()*imdForces->getElementSize(), stream);
    cuEventRecord(syncEvent, stream);
}

double CudaCalcImdForceKernel::addForces(bool includeForces, bool includeEnergy, int groups) {
    if ((groups&forceGroupFlag) == 0)
        return 0;

    // Wait until executeOnWorkerThread() is finished.
    
    cu.getWorkThread().flush();
    cuStreamWaitEvent(cu.getCurrentStream(), syncEvent, 0);

    // Add in the forces.
    IF_DEBUG std::cerr << "IMD CUDA: Adding forces " << std::endl;

    if (includeForces) {
        void* args[] = {&imdForces->getDevicePointer(), &cu.getForce().getDevicePointer(), &cu.getAtomIndexArray().getDevicePointer()};
        cu.executeKernel(addForcesKernel, args, cu.getNumAtoms());
    }
    
    // Return the energy.
    
    double energy = 0;
    return energy;
}
