/* -------------------------------------------------------------------------- *
 *                                OpenMMImd                                 *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2016 Stanford University and the Authors.           *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

#include "ImdForceProxy.h"
#include "ImdForce.h"
#include "openmm/serialization/SerializationNode.h"

using namespace ImdPlugin;
using namespace OpenMM;
using namespace std;

ImdForceProxy::ImdForceProxy() : SerializationProxy("ImdForce") {
}

void ImdForceProxy::serialize(const void* object, SerializationNode& node) const {
    node.setIntProperty(versionKey, 1);
    const ImdForce& force = *reinterpret_cast<const ImdForce*>(object);
    node.setStringProperty(addressKey, force.getAddress());
    node.setIntProperty(portKey, force.getPort());
    node.setBoolProperty(waitKey, force.willWaitForConnection());
    node.setIntProperty(rateKey, force.getRate());
    node.setBoolProperty(internalUnitsKey, force.usesInternalUnits());
}

void* ImdForceProxy::deserialize(const SerializationNode& node) const {
    if (node.getIntProperty("version") != 1)
        throw OpenMMException("Unsupported version number");

    const auto& address = node.getStringProperty(addressKey);
    auto port = node.getIntProperty(portKey);
    auto wait = node.getBoolProperty(waitKey);
    auto rate = node.getIntProperty(rateKey);
    auto internalUnits = node.getBoolProperty(internalUnitsKey);

    auto* force = new ImdForce(address, port, wait, rate, internalUnits);
    return force;
}
