/***************************************************************************
 *cr
 *cr            (C) Copyright 1995-2003 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/***************************************************************************
 * RCS INFORMATION:
 *
 *      $RCSfile: vmdsock.c,v $
 *      $Author: johns $        $Locker:  $             $State: Exp $
 *      $Revision: 1.1 $      $Date: 2003/09/12 18:30:46 $
 *
 ***************************************************************************
 * DESCRIPTION:
 *   Socket interface, abstracts machine dependent APIs/routines.
 ***************************************************************************/

#define VMDSOCKINTERNAL 1


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if !defined(_MSC_VER)
#include <arpa/inet.h>
#include <fcntl.h>

#include <sys/types.h>
#include <unistd.h>   /* for Linux */
#include <sys/socket.h>
#include <netdb.h>
#else 
#include <winsock2.h>
#endif

#include <errno.h>
#include <stdexcept>
#include <sstream>

#include "imd/vmdsock.h"

int vmdsock_init(void) {
#if defined(_MSC_VER)
    int rc = 0;
  static int initialized = 0;

  if (!initialized) {
    WSADATA wsdata;
    rc = WSAStartup(MAKEWORD(1, 1), &wsdata);
    if (rc == 0)
      initialized = 1;
  }

  return rc;
#else
    return 0;
#endif
}

void reset_address(VmdSocket& v)
{
    memset(&(v.addr), 0, sizeof(v.addr));
}

VmdSocket vmdsock_create(void) {

    auto c_socket = socket(PF_INET, SOCK_STREAM, 0);
    if(c_socket == -1) {
        throw std::runtime_error("Failed to open socket.");
    }
    auto socket = VmdSocket();
    socket.sd = c_socket;
    return socket;
}

int  vmdsock_connect(VmdSocket& v, std::string host, int port) {
    char address[1030];
    struct hostent *h;

    h = gethostbyname(host.c_str());
    if(h == NULL) {
        std::stringstream ss;
        ss << "Failed to resolve host." << host << std::endl;
        throw std::runtime_error(ss.str());
    }
    sprintf(address, "%d.%d.%d.%d",
            (unsigned char)h->h_addr_list[0][0],
            (unsigned char)h->h_addr_list[0][1],
            (unsigned char)h->h_addr_list[0][2],
            (unsigned char)h->h_addr_list[0][3]);

    reset_address(v);
    v.addr.sin_family = PF_INET;
    v.addr.sin_addr.s_addr = inet_addr(address);
    v.addr.sin_port = htons(port);

    return connect(v.sd, (struct sockaddr *) &v.addr, sizeof(v.addr));
}

int vmdsock_bind(VmdSocket& v, int port) {
    reset_address(v);
    v.addr.sin_family = PF_INET;
    v.addr.sin_port = htons(port);

    return bind(v.sd, (struct sockaddr *) &v.addr, sizeof(v.addr));
}

int vmdsock_listen(VmdSocket& v) {
    return listen(v.sd, 5);
}

VmdSocket vmdsock_accept(VmdSocket&  v) {
#if defined(SOCKLEN_T)
    SOCKLEN_T len;
#elif defined(ARCH_LINUXALPHA) || defined(__APPLE__) || defined(__linux__)
    socklen_t len;
#else
    int len;
#endif
    VmdSocket new_s;
    int rc;
    len = sizeof(v.addr);
    rc = accept(v.sd, (struct sockaddr *) &v.addr, &len);
    if (rc >= 0) {
        new_s = v;
        new_s.sd = rc;
    }
    return new_s;
}

int  vmdsock_write(VmdSocket&  v, const void *buf, int len) {
    int result;
#if defined(_MSC_VER)
    result = send(v.sd, (const char*)buf, len, 0);  // windows lacks the write() call
#else
    result = write(v.sd, buf, len);
#endif
    if(result < 0)
        throw_socket_error("write");
    return result;
}

void throw_socket_error(std::string operation) {
    std::stringstream ss;
    ss << "Socket error during " << operation << " Error code: " << errno << std::endl;
    throw std::runtime_error(ss.str());
}

int  vmdsock_read(VmdSocket&  v, void *buf, int len) {
    int result;
#if defined(_MSC_VER)
    result = recv(v.sd, (char*)buf, len, 0); // windows lacks the read() call
#else
    result = read(v.sd, buf, len);
#endif
    if(result < 0)
        throw_socket_error("read");
    return result;
}

void vmdsock_shutdown(VmdSocket& v) {
#if defined(_MSC_VER)
    shutdown(v.sd, SD_SEND);
#else
    shutdown(v.sd, 1);  /* complete sends and send FIN */
#endif
}

void vmdsock_close(VmdSocket&  v) {
#if defined(_MSC_VER)
    closesocket(v.sd);
#else
    close(v.sd);
#endif
}

int vmdsock_selread(VmdSocket& v, int sec, int microsec) {
    fd_set rfd;
    struct timeval tv;
    int rc;

    FD_ZERO(&rfd);
    FD_SET(v.sd, &rfd);
    memset((void *)&tv, 0, sizeof(struct timeval));
    tv.tv_sec = sec;
    tv.tv_usec = microsec;
    do {
        rc = select(v.sd + 1, &rfd, NULL, NULL, &tv);
    } while (rc < 0 && errno == EINTR);
    if(rc < 0)
    {
        throw_socket_error("select read");
    }

    return rc;
}

int vmdsock_selwrite(VmdSocket& v, int sec, int microsec) {
    fd_set wfd;
    struct timeval tv;
    int rc;

    FD_ZERO(&wfd);
    FD_SET(v.sd, &wfd);
    memset((void *)&tv, 0, sizeof(struct timeval));
    tv.tv_sec = sec;
    tv.tv_usec = microsec;
    do {
        rc = select(v.sd + 1, NULL, &wfd, NULL, &tv);
    } while (rc < 0 && errno == EINTR);
    if(rc < 0)
    {
        throw_socket_error("select write");
    }
    return rc;
}
