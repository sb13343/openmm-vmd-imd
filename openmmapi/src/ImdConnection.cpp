#include <utility>
#include <imd/vmdsock.h>
#include <imd/imd.h>
#include <iostream>
#include <vector>
#include <tuple>

//
// Created by Mike O'Connor on 2019-11-26.
//

#include "imd/ImdConnection.h"

ImdConnection::ImdConnection()
{
    socketInitialised = false;
    clientConnected = false;
}

ImdConnection::~ImdConnection() {
    disconnectClient();
    if(socketInitialised)
        vmdsock_close(sock);
}

int ImdConnection::getPort() const {
    if(socketInitialised)
        return port;
    throw std::runtime_error("Socket not initialised.");
}

const std::string & ImdConnection::getAddress() const {
    if(socketInitialised)
        return address;
    throw std::runtime_error("Socket not initialised.");
}

bool ImdConnection::isClientConnected() const {
    return clientConnected;
}

void ImdConnection::initialiseServer(const std::string& address, int port) {
    this->address = address;
    this->port = port;

    vmdsock_init();
    sock = vmdsock_create();
    vmdsock_bind(sock, port);
    vmdsock_listen(sock);
    socketInitialised = true;
    std::cout << "Established socket at " << address.c_str() << ":" << port << std::endl;
}

bool ImdConnection::tryConnect(bool wait) {
    if(!socketInitialised)
        throw std::runtime_error("IMD socket not initialised!");

    if (wait && !clientConnected)
    {
        std::cout << "Waiting for IMD connection at " << address.c_str() << ":" << port << std::endl;
    }
    do
    {
        tryConnectToClient();
    } while (wait && !clientConnected);
    return clientConnected;
}


void ImdConnection::tryConnectToClient() {
    if (vmdsock_selread(sock, 0, 0) > 0)
    {
        clientsock = vmdsock_accept(sock);
        clientConnected = true;
        std::cerr << "Performing handshake" << std::endl;
        if (imd_handshake(clientsock) != 0)
        {
            std::cerr << "Client failed handshake" << std::endl;
            vmdsock_shutdown(clientsock);
            clientConnected = false;
        }

        std::cerr << "Client connected, awaiting GO command" << std::endl;
        if (clientConnected)
        {
            bool receivedGoCommand = receiveGoCommand();
            if (!receivedGoCommand)
                std::cerr << "Client did not send GO command" << std::endl;
            else
                std::cout << "Received GO command, starting IMD!" << std::endl;
        }
    }
}

void ImdConnection::disconnectClient() {
    if(clientConnected)
        vmdsock_shutdown(clientsock);
    clientConnected = false;
}


bool ImdConnection::receiveGoCommand()
{
    //Wait a couple of seconds to receive the GO command.
    int goTimeout=2;
    auto [received, type, length] = receiveHeader(goTimeout);
    return type == IMD_GO;
}



void ImdConnection::sendPositions(const std::vector<float> &positions)
{
    if(!clientConnected)
        return;

    try
    {
        imd_send_fcoords(clientsock, positions.size() / 3, &positions[0]);
    }
    catch(std::runtime_error& e)
    {
        std::cerr << "Error during sending positions, disconnecting client: " << e.what() << std::endl;
        disconnectClient();
    }
}

void ImdConnection::sendEnergies(const IMDEnergies& energies)
{
    if(!clientConnected)
        return;
    imd_send_energies(clientsock, &energies);
}


void ImdConnection::receiveMDCommunication(int numForces, std::vector<int> &indices, std::vector<float> &forces)
{
    if(!clientConnected)
        return;
    try
    {
        imd_recv_mdcomm(clientsock, numForces, &indices[0], &forces[0]);
    }
    catch (std::runtime_error& e)
    {
        std::cerr << "Error during MD communication, disconnecting client: " << e.what() << std::endl;
        disconnectClient();
    }
}

std::tuple<bool, IMDType, int> ImdConnection::receiveHeader(int timeoutSeconds, int timeoutMicroseconds)
{
    if(!isClientConnected())
        return std::tuple<bool, IMDType, int>(false, IMDType(), 0);
    int length;
    int x = vmdsock_selread(clientsock, timeoutSeconds, timeoutMicroseconds);
    if(x <= 0)
        return {false, IMDType(), 0};
    try
    {
        IMDType type = imd_recv_header(clientsock, &length);
        return {true, type, length};
    }
    catch (std::runtime_error& e)
    {
        std::cerr << "Error during message receiving, disconnecting client: " << e.what() << std::endl;
        disconnectClient();
        return {false, IMDType(), 0};
    }
}


