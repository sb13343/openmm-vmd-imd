/***************************************************************************
 *cr
 *cr            (C) Copyright 1995-2003 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/***************************************************************************
 * RCS INFORMATION:
 *
 *      $RCSfile: imd.h,v $
 *      $Author: johns $       $Locker:  $             $State: Exp $
 *      $Revision: 1.1 $       $Date: 2003/09/12 18:30:46 $
 *
 ***************************************************************************/

#ifndef IMD_H__
#define IMD_H__

#include <limits.h>
#include "vmdsock.h"

#if ( INT_MAX == 2147483647 )
typedef int     int32;
#else
typedef short   int32;
#endif

#ifdef _WIN32
#  ifdef MODULE_API_EXPORTS
#    define MODULE_API __declspec(dllexport)
#  else
#    define MODULE_API __declspec(dllimport)
#  endif
#else
#  define MODULE_API
#endif

typedef enum IMDType_t {
    IMD_DISCONNECT,   /** close IMD connection, leaving sim running */
    IMD_ENERGIES,     /** energy data block                         */
    IMD_FCOORDS,      /** atom coordinates                          */
    IMD_GO,           /** start the simulation                      */
    IMD_HANDSHAKE,    /** endianism and version check message       */
    IMD_KILL,         /** kill the simulation job, shutdown IMD     */
    IMD_MDCOMM,       /** MDComm style force data                   */
    IMD_PAUSE,        /** pause the running simulation              */
    IMD_TRATE,        /** set IMD update transmission rate          */
    IMD_IOERROR,       /** indicate an I/O error                     */
} IMDType;

#define IMDENERGIESSIZE 10 *4

typedef struct {
    int32 tstep = 0;      /**< integer timestep index                    */
    float T = 0.0f;          /**< Temperature in degrees Kelvin             */
    float Etot = 0.0f;       /**< Total energy, in Kcal/mol                 */
    float Epot = 0.0f;       /**< Potential energy, in Kcal/mol             */
    float Evdw = 0.0f;       /**< Van der Waals energy, in Kcal/mol         */
    float Eelec = 0.0f;      /**< Electrostatic energy, in Kcal/mol         */
    float Ebond = 0.0f;      /**< Bond energy, Kcal/mol                     */
    float Eangle = 0.0f;     /**< Angle energy, Kcal/mol                    */
    float Edihe = 0.0f;      /**< Dihedral energy, Kcal/mol                 */
    float Eimpr = 0.0f;      /**< Improper energy, Kcal/mol                 */
} IMDEnergies;      /**<                                           */

/* Send control messages - these consist of a header with no subsequent data */
/**
 * Send disconnect message, leaving the simulation running but disconnecting the client.
 * @return 0 if no error occurs.
 */
int imd_disconnect(VmdSocket&);

/**
 * Send Pause command, halting the simulation.
 * @return 0 if no error occurs.
 */
int imd_pause(VmdSocket&);
/**
 * Send Kill command, halting the simulation.
 * @return 0 if no error occurs.
 */
int imd_kill(VmdSocket&);
/**
 * Send IMD handshake message, which establishes endianness and checks version compatibility.
 * @return 0 if no error occurs.
 */
int imd_handshake(VmdSocket&);

/**
 * Send new transmission rate, the rate at which IMD will send new positions.
 * @return 0 if no error occurs.
 */
int imd_trate(VmdSocket&, int32); /** set IMD update transmission rate */

/* Send data update messages */
/**
 * Send IMD force communication, consisting of array of indices and array of forces.
 * @return 0 if no error occurs.
 */
int imd_send_mdcomm(VmdSocket&, int32, const int32 *, const float *);

/**
 * Serialize IMD energies into a buffer
 * @param buffer Buffer to serialize into, must be of correct size.
 * @param energies Pointer to IMD energies struct to serialize.
 * @return Pointer at the end of the buffer, after serialization.
 */
MODULE_API char * imd_serialize_energies(char* buffer, const IMDEnergies* energies);

/**
 * Serialize IMD energies into a buffer
 * @param buffer Buffer to deserialize from, must be of correct size.
 * @param energies Pointer to IMD energies struct to deserialize into.
 * @return Pointer at the end of the buffer, after deserialization.
 */
MODULE_API char * imd_deserialize_energies(char* buffer, IMDEnergies* energies);

/**
 * Send the energies struct
 * @return 0 if no error occurs.
 */
int imd_send_energies(VmdSocket&, const IMDEnergies *);

/**
 * Send the IMD positions (floating coords).
 * @return 0 if no error occurs.
 */
int imd_send_fcoords(VmdSocket&, int32, const float *);

/**
 *  Receive the handshake message.
 *  @return if server and client have the same relative endianism, 1 if they have opposite endianism, and -1 if there
 *  was an error in the handshake process.
 */
int imd_recv_handshake(VmdSocket&);

/**
 * Receive header and any associated payload.
 * @return Type of message received.
 */
IMDType imd_recv_header(VmdSocket&, int32 *);

/**
 *  Receive MDComm-style forces, consisting of array of atom indices and cartesian forces.
 *  Forces are in Kcal/mol/angstrom
 *  @return 0 if no error occurs.
 */
int imd_recv_mdcomm(VmdSocket&, int32, int32 *, float *);

/**
 * Receive IMD energy struct.
 * @return 0 if no error occurs.
 */
int imd_recv_energies(VmdSocket&, IMDEnergies *);

/**
 *  Receive atom coordinates and forces
 *  Forces are in Kcal/mol/angstrom
 */
int imd_recv_fcoords(VmdSocket&, int32, float *);


#endif