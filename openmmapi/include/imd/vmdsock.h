/***************************************************************************
 *cr
 *cr            (C) Copyright 1995-2003 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/***************************************************************************
 * RCS INFORMATION:
 *
 *      $RCSfile: vmdsock.h,v $
 *      $Author: johns $        $Locker:  $             $State: Exp $
 *      $Revision: 1.1 $      $Date: 2003/09/12 18:30:46 $
 *
 ***************************************************************************
 * DESCRIPTION:
 *   socket interface layer, abstracts platform-dependent routines/APIs
 ***************************************************************************/

#ifndef VMD_IMD_VMDSOCK_H
#define VMD_IMD_VMDSOCK_H

#include <string>
#if !defined(_MSC_VER)
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <sys/file.h>
#else
#ifndef VMD_WINSOCK
#define VMD_WINSOCK
#pragma comment(lib, "Ws2_32.lib")
#include <windows.h>
//#include <winsock2.h>
#endif
#endif

typedef struct {
    struct sockaddr_in addr; /* address of socket provided by bind() */
    int addrlen;             /* size of the addr struct */
    int sd;                  /* socket file descriptor */
} VmdSocket;

int   vmdsock_init(void);
VmdSocket vmdsock_create(void);
int   vmdsock_bind(VmdSocket&, int);
int   vmdsock_listen(VmdSocket&);
VmdSocket vmdsock_accept(VmdSocket&);  /* return new socket */
int   vmdsock_connect(VmdSocket& v, std::string host, int port);
int   vmdsock_write(VmdSocket&, const void *, int);
int   vmdsock_read(VmdSocket&, void *, int);
int   vmdsock_selread(VmdSocket&, int, int );
int   vmdsock_selwrite(VmdSocket&, int, int);
void  vmdsock_close(VmdSocket&);
void  vmdsock_shutdown(VmdSocket&);
void  vmdsock_destroy(VmdSocket&);
void throw_socket_error(std::string string);

#endif

