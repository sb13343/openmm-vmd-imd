//
// Created by Mike O'Connor on 2019-11-26.
//

#ifndef OPENMM_VMD_IMD_IMDCONNECTION_H
#define OPENMM_VMD_IMD_IMDCONNECTION_H


#include <string>
#include <optional>
#include <imd/vmdsock.h>
#include <imd/imd.h>
#include <vector>
#include "internal/windowsExportImd.h"

/**
 * Represents an Imd connection, abstracting the communication away
 * from the underlying transport details.
 */
class OPENMM_EXPORT_IMD ImdConnection {
public:
    ImdConnection();
    ~ImdConnection();

    /**
     * Initialises the IMD server.
     * @param address Address to bind to.
     * @param port Port to server on.
     */
    void initialiseServer(const std::string& address, int port);

    /**
     * Attempt to connect a client.
     * @param wait Whether to wait for a connection.
     * @return True if a connection is made, false otherwise.
     */
    bool tryConnect(bool wait = false);

    /**
     * Get the address this IMD server is serving on.
     * @return The address this IMD server is serving on.
     */
    const std::string & getAddress() const;

    /**
     * Get the port this IMD server is serving on.
     * @return The port this IMD server is serving on.
     */
    int getPort() const;

    /**
     * Indicate whether a client is currently connected to this server or not.
     * @return True if a client is connected, false otherwise.
     */
    bool isClientConnected() const;

    /**
     * Send the positions to a connected client.
     * @param positions The atomic positions to transmit.
     * @throws runtime exception if no client is connected.
     */
    void sendPositions(const std::vector<float> &positions);

    /**
     * Send the energies to a connected client.
     * @param energies IMD energy struct to send.
     * @throws runtime exception if no client is connected.
     */
    void sendEnergies(const IMDEnergies &energies);

    /**
     * Receives forces from an IMD client.
     * @param numForces Number of forces to receive, determined by the header that precedes calls to this method.
     * @param indices The indices of the atoms to apply forces to.
     * @param forces The forces to be applied.
     */
    void receiveMDCommunication(int numForces, std::vector<int> &indices, std::vector<float> &forces);

    /**
     * Receive an IMD header from an IMD client.
     * @param timeoutSec Timeout to receive for, in seconds.
     * @param timeoutMicrosec Timeout to receive for, in microseconds.
     * @return A tuple indicating whether a header was received, the type of message received, and an associated
     * integer, the meaning of which depends on the type of message.
     */
    std::tuple<bool, IMDType, int> receiveHeader(int timeoutSec=0, int timeoutMicrosec = 0);

    /**
     * Disconnect any connected client.
     */
    void disconnectClient();

private:
    bool socketInitialised{};
    VmdSocket sock{};
    bool clientConnected{};
    VmdSocket clientsock{};
    std::string address;
    int port{};

    void tryConnectToClient();

    bool receiveGoCommand();
};


#endif //OPENMM_VMD_IMD_IMDCONNECTION_H
