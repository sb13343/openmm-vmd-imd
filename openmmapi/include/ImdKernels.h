#ifndef IMD_KERNELS_H_
#define IMD_KERNELS_H_

/* -------------------------------------------------------------------------- *
 *                                   OpenMM                                   *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2016 Stanford University and the Authors.           *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

#include "ImdForce.h"
#include "openmm/KernelImpl.h"
#include "openmm/Platform.h"
#include "openmm/System.h"
#include "debug.h"
#include <string>
#include <vector>
#include <iostream>

namespace ImdPlugin {

/**
 * This kernel is invoked by ImdForce to calculate the forces acting on the system and the energy of the system.
 * This base kernel implements all of the features shared between platforms, while subclasses implement the details.
 */
class OPENMM_EXPORT_IMD CalcImdForceKernel : public OpenMM::KernelImpl {

protected:

    /**
     * Indicates whether the simulation is currently paused.
     * @return True if the simulation is paused, false otherwise.
     */
    bool isPaused();

    /**
     * Set a new transmission rate.
     * @param newRate
     */
    void setRate(int newRate);

    bool hasInitialized;
    ImdConnection imd;
    bool waitForConnection = false;
    int rate = 1;
    bool paused = false;
    bool internalUnits = false;
    double positionConversion = 1;
    double forceConversion = 1;

    std::vector<int> receivedAtomIndices;
    std::vector<float> receivedForces;

    void setupUnitConversions(bool useInternalUnits);

    void updatePositionsToSend(const std::vector<OpenMM::Vec3>& positions);

public:
    static std::string Name() {
        return "CalcImdForce";
    }

    CalcImdForceKernel(std::string name, const OpenMM::Platform& platform);

    /**
     * Initialize the kernel.
     *
     * @param system     the System this kernel will be applied to
     * @param force      the ImdForce this kernel will be used for
     */
    virtual void initialize(const OpenMM::System& system, const ImdForce& force) = 0;
    /**
     * Execute the kernel to calculate the forces and/or energy.
     *
     * @param context        the context in which to execute this kernel
     * @param includeForces  true if forces should be calculated
     * @param includeEnergy  true if the energy should be calculated
     * @return the potential energy due to the force
     */
    virtual double execute(OpenMM::ContextImpl& context, bool includeForces, bool includeEnergy) = 0;

    /**
     * Initialises the Imd server.
     *
     * @param system the system to base the Imd server parameters from.
     * @param force the Imd force to extract server parameters from.
     */
    void initializeImd(const OpenMM::System& system, const ImdForce& force);

    void receiveForces(int numForces);

    /**
     * Processes any Imd messages received from the client.
     * @return The type of IMD message received. If no message received or the client is not connected
     * Disconnect type is returned.
     */
    IMDType processImdInput();

    void sendPositions();

    std::vector<float> positionsToSend;

    void clearReceiveArrays();

    void processImdMessage(IMDType messageType, int payload);
};

} // namespace ImdPlugin

#endif /*IMD_KERNELS_H_*/
