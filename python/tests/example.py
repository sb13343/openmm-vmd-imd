from simtk.openmm import app
import simtk.openmm as mm
from simtk import unit
from sys import stdout
import openmmimd

pdb = app.PDBFile('alanine-dipeptide.pdb')
forcefield = app.ForceField('amber99sbildn.xml')

system = forcefield.createSystem(pdb.topology, nonbondedMethod=app.NoCutoff,
    nonbondedCutoff=1.0*unit.nanometers, constraints=app.HBonds,
    ewaldErrorTolerance=0.0005)
integrator = mm.LangevinIntegrator(300*unit.kelvin, 1.0/unit.picoseconds,
    2.0*unit.femtoseconds)
integrator.setConstraintTolerance(0.00001)

#add the IMD force.
imdForce = openmmimd.ImdForce('localhost', 9000)
system.addForce(imdForce)

platform = mm.Platform.getPlatformByName('Reference')
simulation = app.Simulation(pdb.topology, system, integrator, platform)
simulation.context.setPositions(pdb.positions)

simulation.reporters.append(app.StateDataReporter(stdout, 1000, step=True,
    potentialEnergy=True, temperature=True,
    speed=True, separator='\t'))

print('Running IMD...')

while True:
    simulation.step(10)
